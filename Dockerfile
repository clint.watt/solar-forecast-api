# Use the official Python 3.11 image as the base
FROM python:3.11

# Set the working directory inside the container
WORKDIR /app

# Copy the requirements file into the container
COPY requirements.txt .

# Install dependencies from requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Expose port 5000 for communication
EXPOSE 80
ENV AZURE_CLIENT_ID="d659e860-961d-4377-9ead-98fa3feb3dec"
ENV AZURE_TENANT_ID="8e48d764-90e8-4522-a22b-7c71ce956aab"
ENV AZURE_CLIENT_SECRET="VV28Q~vlvHPjsSP0O3IBbA_gIeWlRorJtV2LJaDG"
# Copy your main.py file into the container
COPY *.py .

# Run flask
CMD ["flask", "--app", "main",  "run", "--port=80", "--host=0.0.0.0"]
