import os
from datetime import datetime

import dask.dataframe as dd
import pandas as pd
import pytz
from cachetools import TTLCache
from dotenv import load_dotenv
from flask import Flask, send_from_directory

from consts import ISO_date_fmt, time_bins

load_dotenv()
cache_25m = TTLCache(maxsize=128, ttl=60 * 25)
app = Flask(__name__)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route("/", methods=['GET'])
@app.route("/<day>", methods=['GET'])
def index(day: str = None) -> object:
    print(f"day: {day}")
    # work out the day to query, default today -------------------------------------------------------------------------
    if day is None:
        query_date = datetime.today()
    else:
        if not day.isdigit() or len(day) != 8:
            return {'error': 'Day must be in format YYYYMMDD'}

        _yyyy, _mm, _dd = int(day[0:4]), int(day[4:6]), int(day[6:8])

        try:
            query_date = datetime(_yyyy, _mm, _dd)
        except ValueError:
            return {'error': 'Not a valid date.'}

    cache_key = query_date.strftime('%Y%m%d')
    cached = cache_25m.get(cache_key)

    if cached is not None:
        return cached

    try:
        # get appropriate pq file --------------------------------------------------------------------------------------
        # dev_or_prod = os.getenv('ENV', 'dev')
        dev_or_prod = "dev"

        dask_df = dd.read_parquet(
            f"abfs://{dev_or_prod}-datalake/meteomatics/solarforecasthh/{query_date.strftime('%Y/%m/%d')}/*.parquet",
            storage_options={"account_name": f"{dev_or_prod}datalakeflexitricity", "anon": False}
        )
        dask_df = dask_df.compute()

        if dask_df.empty:
            return {'error': 'No data was retrieved'}

        # add a uk/local dt version of forecast time for ease of EFA calcs ---------------------------------------------
        uk_tz = pytz.timezone("Europe/London")
        dask_df["dt_uk"] = pd.to_datetime(dask_df["dt_utc"]).dt.tz_convert(uk_tz)

        # remove utc tz part: make dt naive and extract the date part to use for grouping return -----------------------
        dask_df['as_of_utc'] = dask_df['as_of_utc'].dt.tz_localize(None)
        dask_df["dt_day"] = dask_df["dt_uk"].dt.date.astype(str)

        # only keep latest of the forecasts for the day ----------------------------------------------------------------
        dask_df.sort_values(by=['as_of_utc'], ascending=False, inplace=True)
        dask_df.drop_duplicates(subset=['lat', 'lon', 'dt_utc'], keep='first', inplace=True)
        dask_df.sort_values(by=['lat', 'dt_utc'], ascending=True, inplace=True)

        # Assign time bins to each row ---------------------------------------------------------------------------------
        dask_df["efa"] = pd.cut(
            dask_df["dt_uk"].dt.hour, bins=[start for _, (start, _) in time_bins] + [24],
            labels=[label for label, _ in time_bins])

        # TODO: bit of a fudge to cover not coding for EFA1 which spans over midnight, but won't have sun anyway, so...
        dask_df = dask_df.dropna(subset=["efa"])

        # Group by 'lat' and time bin, then calculate average solar power per efa prop ---------------------------------
        efa_blocks = dask_df.groupby(["dt_day", "lat", "efa"], observed=False)["solar_power_kw"].mean().reset_index()
        efa_blocks.rename(columns={'solar_power_kw': 'mean_solar_power_kw'}, inplace=True)
        efa_blocks.sort_values(by=["dt_day", "lat", "efa"], ascending=True, inplace=True)
        efa_blocks = efa_blocks.to_dict(orient='records')

        # stringify the times for pretty json return -------------------------------------------------------------------
        dask_df['dt_utc'] = dask_df['dt_utc'].dt.strftime(ISO_date_fmt)
        dask_df['as_of_utc'] = dask_df['as_of_utc'].dt.strftime(ISO_date_fmt)
        dask_df['dt_uk'] = dask_df['dt_uk'].dt.strftime(ISO_date_fmt)
        dask_df = dask_df.to_dict(orient='records')

        out = {'breakdown': dask_df, 'efa_breakdown': efa_blocks}
        cache_25m[cache_key] = out

        return out

    except Exception as e:
        print(e)
        return {'error': e}
