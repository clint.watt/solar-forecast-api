# Define efa time bins
time_bins = [
    (2, (3, 7)),
    (3, (7, 11)),
    (4, (11, 15)),
    (5, (15, 19)),
    (6, (19, 23)),
]

ISO_date_fmt = '%Y-%m-%d %H:%M:%S'
